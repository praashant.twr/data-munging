from calculation import CalcDifference


class TempratureSpread(CalcDifference):
    ''' initializeand compute temperature spread '''
    def __init__(self, location, first_index, second_index, display_index, char_seperator):
        super(TempratureSpread, self).__init__(location, first_index,
                                               second_index, display_index, char_seperator)

    def calculation(self):
        result = super().find_min_difference()
        return result


class GoalDifference(CalcDifference):
    '''initialize and compute goal diffrence '''
    def __init__(self, location, first_index, second_index, display_index):
        super(GoalDifference, self).__init__(location, first_index,
                                             second_index, display_index)

    def calculation(self):
        result = super().find_min_difference()
        return result
