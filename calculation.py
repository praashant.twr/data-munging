from extraction import File


class CalcDifference(File):
    def __init__(self, location, index_1, index_2, display_index, special_char=None):
        super(CalcDifference, self).__init__(location)
        self.i = index_1
        self.j = index_2
        self.d = display_index
        self.special_char = special_char

    def find_min_difference(self):
        diffrence = None
        display = None
        data = super().data_extraction()
        for line in data:
            if self.special_char != None:
                first = float(line[self.i].split(self.special_char)[0])
                second = float(line[self.j].split(self.special_char)[0])
            else:
                first = float(line[self.i])
                second = float(line[self.j])

            if diffrence == None or diffrence > abs(first - second):
                diffrence = abs(first - second)
                display = line[self.d]
        return diffrence, display
