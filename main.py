from app import GoalDifference
from app import TempratureSpread

VALUE_INDEX1 = 6
VALUE_INDEX2 = 8
DISPLAY_INDEX = 1
RESULT = GoalDifference('football.dat', VALUE_INDEX1, VALUE_INDEX2, DISPLAY_INDEX).calculation()

print('goal diffrence:', RESULT[0], '   team:', RESULT[1])


VALUE_INDEX1 = 2
VALUE_INDEX2 = 1
DISPLAY_INDEX = 0
SPECIAL_CHAR = '*'
RESULT = TempratureSpread('weather.dat', VALUE_INDEX1,
                          VALUE_INDEX2, DISPLAY_INDEX, SPECIAL_CHAR).calculation()

print('temperature spread:', RESULT[0], '  day:', RESULT[1])
