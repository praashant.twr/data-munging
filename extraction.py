class File:
    def __init__(self, location):
        self.location = location

    def data_extraction(self):
        with open(self.location, 'r') as file:
            next(file)
            data = []
            for line in file:
                if len(line.strip().split()) > 1:
                    data.append(line.strip().split())
        return data
